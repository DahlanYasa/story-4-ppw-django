from django.shortcuts import render, redirect
from .forms import JadwalForm
from .models import Jadwal
# Create your views here.

def buat_jadwal(request):
    form = JadwalForm(request.POST or None)
    if form.is_valid():
        form.save()
        # form = JadwalForm()
        return redirect('list_jadwal')  

    context = {
        'form':form
    }

    return render(request, 'jadwal/buat_jadwal.html', context)

def list_jadwal(request):
    obj = Jadwal.objects.all()

    context = {
        'jadwals':obj
    }

    return render(request, 'jadwal/list_jadwal.html', context)

def delete(request, delete_id):
    Jadwal.objects.filter(id=delete_id).delete()
    return redirect('list_jadwal')
